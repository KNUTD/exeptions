class IllegalArgumentException extends Error {
    constructor(arg) {
        super();
        this.message = `${JSON.stringify(arg)} is not valid argument`;
        this.name = 'IllegalArgumentException';
        this.stack = (new Error()).stack;
    }
}

const validateNumericArgs = args => {
    const wrongArgument = args.find(arg => typeof arg !== 'number');
    verification(wrongArgument);
};

const validateObjArgs = (obj, ...args) => {
    const wrongArgument = args.find(arg => !(arg instanceof obj));
    verification(wrongArgument);
};

const verification = wrongArgument => {
    if (wrongArgument) {
        throw new IllegalArgumentException(wrongArgument);
    }
};


class Point {
    constructor(x, y) {
        validateNumericArgs([x, y]);
        this.x = x;
        this.y = y;
    }

    static random(xFrom = 0, xTo = 500, yFrom = 0, yTo = 500) {
        validateNumericArgs([xFrom, xTo, yFrom, yTo]);
        return new Point(Point.randomNumber(xFrom, xTo), Point.randomNumber(yFrom, yTo));
    }

    static randomNumber(min, max) {
        validateNumericArgs([min, max]);
        return Math.random() * (max - min) + min;
    }

    drawing(ctx) {
        ctx.fillStyle = "red";
        ctx.fillRect(this.x, this.y, 1, 1);
    }
}

class Line {
    constructor(firstPoint, secondPoint){
        validateObjArgs(Point, firstPoint, secondPoint);
        this.firstPoint = firstPoint;
        this.secondPoint = secondPoint;
    }

    length() {
        return Math.sqrt((this.firstPoint.x - this.secondPoint.x) ** 2 + (this.firstPoint.y - this.secondPoint.y) ** 2);
    }
}

class Rectangle {
    constructor(firstPoint, secondPoint) {
        validateObjArgs(Point, firstPoint, secondPoint);
        this.left = Math.min(firstPoint.x, secondPoint.x);
        this.right = Math.max(firstPoint.x, secondPoint.x);
        this.bottom = Math.max(firstPoint.y, secondPoint.y);
        this.top = Math.min(firstPoint.y, secondPoint.y);
        this.height = this.bottom - this.top;
        this.width = this.right - this.left;
    }

    isSquare() {
        return this.width === this.height;
    }

    contains(point) {
        validateObjArgs(Point, point);
        return (this.left < point.x && this.right > point.x) || (this.top < point.y && this.bottom > point.y)

    }

    area() {
        return this.width * this.height;
    }

    drawing(ctx) {
        ctx.strokeStyle = "green";
        ctx.strokeRect(this.left, this.top, this.width, this.height);
        ctx.save();
    };
}

class Triangle {
    constructor(rectangle) {
        validateObjArgs(Rectangle, rectangle);
        this.leftX = rectangle.left;
        this.leftY = rectangle.top + rectangle.height;
        this.rightX = rectangle.right;
        this.rightY = rectangle.bottom;
        this.topX = rectangle.left + (rectangle.width / 2);
        this.topY = rectangle.top;
        this.height = rectangle.height;
        this.base = rectangle.width;
        this.side = Math.sqrt(this.height * this.height + (this.base / 2) * (this.base / 2));
    }

    area() {
        return (this.base * this.height) / 2;
    }

    drawing(ctx) {
        ctx.strokeStyle = "red";
        ctx.moveTo(this.leftX, this.leftY);
        ctx.lineTo(this.rightX, this.rightY);
        ctx.lineTo(this.topX, this.topY);
        ctx.lineTo(this.leftX, this.leftY);
        ctx.stroke();
        ctx.save();
    };
}

class Circle {
    constructor(triangle) {
        validateObjArgs(Triangle, triangle);
        this.r = Circle.radius(triangle);
        this.x0 = triangle.topX;
        this.y0 = triangle.topY - this.r + triangle.height;
    }

    static radius(triangle) {
        validateObjArgs(Triangle, triangle);
        return triangle.base / 2 * Math.sqrt((2 * triangle.side - triangle.base) / (2 * triangle.side + triangle.base));
    }

    drawing(ctx) {
        ctx.beginPath();
        ctx.arc(this.x0, this.y0, this.r, 0, 2 * Math.PI, false);
        ctx.fillStyle = "#D3D3D3";
        ctx.fill();
        ctx.restore();
        ctx.closePath();
    }
}

const canvas = document.getElementById("canvas");
const ctx = canvas.getContext("2d");
const rect = new Rectangle(Point.random(), Point.random());
const triangle = new Triangle(rect);
const circle = new Circle(triangle);
triangle.drawing(ctx);
rect.drawing(ctx);
circle.drawing(ctx);
